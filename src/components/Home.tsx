import Link from 'next/link'
import React from 'react'

const Home = () => {
  return (
    <div >
      <nav className='bg-slate-500 p-4 mt-4'>
        <h1 className='inline-block text-black font-bold text-3xl'>To Do App </h1>
        <h1>sample adding for praticse purpose</h1
        <ol className='float-right flex  relative -top-3  mr-32 text-white text-xl'>
         <Link href={'/'}> <li className='m-4 p-1'>Home</li></Link>
         <Link href={'/Data'}> <li className='m-4 p-1'>Data</li></Link>
         <Link href={'/signin'}> <li className='m-4 p-1'>Signin</li></Link>
         <Link href={'/login'}> <li className='m-4 p-1'>Login</li></Link>
        </ol>
      </nav>
    </div>
  )
}

export default Home
