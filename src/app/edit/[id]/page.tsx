"use client"
import { Fragment, useEffect, useState,ChangeEvent, FormEvent } from 'react'
import {useRouter, useSearchParams} from 'next/navigation';

export default function Home() {
  const [id,setId]=useState<string|null>("")
  const [name,setName]=useState<string>("");
  const [age,setAge]=useState<string>("");
  const [department,setDepartment]=useState<string>("");
  const [address,setAddress]=useState<string>("");
  const search=useSearchParams()
  const router=useRouter()
  
 useEffect(()=>{
  setId(search.get("_id"))
   setName(search.get("Name"))
   setAge(search.get("Age"))
   setDepartment(search.get("Department"))
   setAddress(search.get("Address"))
 },[])
 
 console.log(name,age,address,department);

 const handleonData=async(event: FormEvent)=>{
  event.preventDefault()
  const response = await fetch(`/api/getdata/${id}`, {
    method: "PATCH",
    body: JSON.stringify({
     name,age,department,address
    }),
  });
  router.push("/Data")

 }
return (
  <Fragment>
 <div className='flex  justify-center  w-1/2 a text-center ml-96  p-2'>
        <form className='justify-self-center' onSubmit={(event)=>handleonData(event)}>
       <h1 className='font-bold text-lg'>Enter a Details</h1> 
        <input type='text'  name="name" placeholder={name} onChange={(e:ChangeEvent<HTMLInputElement>)=>setName(e.target.value)} className='border-2 rounded-xl border-black m-2' /> <br/> 
        <input type="number" name="age" placeholder={age} onChange={(e:ChangeEvent<HTMLInputElement>)=>setAge(e.target.value)} className='border-2 rounded-xl border-black m-2'  /><br/>
        <input type='text' name="dept"placeholder={department} onChange={(e:ChangeEvent<HTMLInputElement>)=>setDepartment(e.target.value)} className='border-2 m-2 rounded-xl border-black' /> <br/>
        <input type='text'name="address" placeholder={address} onChange={(e:ChangeEvent<HTMLInputElement>)=>setAddress(e.target.value)} className='border-2 m-2 rounded-xl border-black' /> <br/> 
        <button className='bg-red-500 text-white rounded-full w-36 hover:bg-sky-800'>submit</button> <br/><br/>
         
       </form>
    
 
     </div>
  </Fragment>
)

}