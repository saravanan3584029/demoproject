

import {connectToDB} from '../../../utils/indexx'
import Prompt from '../../../../models/home'

export const GET = async (request, { params }) => {
    try {
         await connectToDB ()
      const prompt= await Prompt.find()
       return new Response(JSON.stringify(prompt), { status: 200 })

    } catch (error) {
        return new Response("Internal Server Error", { status: 500 });
    }
}
