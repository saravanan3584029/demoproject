import {connectToDB} from '../../../../utils/indexx'
import Prompt from '../../../../../models/home'
export const DELETE = async (request, { params }) => {
    console.log(params.id);
    
     try {
        await connectToDB();
         const Response= await Prompt.deleteOne({_id:params.id});
         console.log("response",Response);
         return new Response("Prompt deleted successfully");

    } catch (error) {
        console.log("errooe",params.id);
        return new Response("Error deleting prompt", { status: 500 });
     }
};


export const PATCH = async (request, { params }) => {
    const {name,age,department,address} =await request.json();

   

    try {
        await connectToDB();
       const existingData = await Prompt.findById(params.id);
        if (!existingData) {
            return new Response("Prompt not found", { status: 404 });
        }
        existingData.Name = name;
        existingData.Age=age,
        // existingData.Department=department
        existingData.Address=address
       const res= await existingData.save();
        return new Response("Successfully updated the Prompts", { status: 200 });
    } catch (error) {
        return new Response("Error Updating Prompt", { status: 500 });
    }
};
