import prompt from '../../../../models/home'
import { connectToDB } from "../../../utils/indexx";


export const POST = async (request) => {

    const { Name,Age,Department,Address } = await request.json();

    try {
         await connectToDB();

         const newPrompt = new prompt({ Name,Age,Department,Address });
        await newPrompt.save();
        return new Response(JSON.stringify(newPrompt), { status: 201 })
    } catch (error) {
        return new Response("Failed to create a new prompt", error);
    }
}
