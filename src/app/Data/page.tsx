"use client";
import Link from "next/link";
import {  useRouter } from "next/navigation";
import React, { useEffect, useState, MouseEvent } from "react";
interface inputThing {
  _id: number;
  Name: string;
  Age: number;
  Department: string;
  Address: string;
}
export default function page() {
  const [data, setData] = useState([]);
  const router = useRouter();
  const fetchPosts = async () => {
    const response = await fetch("/api/getdata");
    const data1 = await response.json();

    setData(data1);
  };

  useEffect(() => {
    fetchPosts();
  }, []);

  const handleupdate = (event: MouseEvent, item: inputThing) => {
    router.push(`/edit/${item._id}`);
  };

  const handledelete = async (event: MouseEvent, item: inputThing) => {
    console.log(item);

    event.preventDefault();
    try {
      await fetch(`/api/getdata/${item._id}`, {
        method: "DELETE",
      });
      fetchPosts();
    } catch (error) {
      console.log("error", error);
    }
  };
  return (
    <div >
      {/* table-auto   text-center border border-slate-400 */}
      {data?.length > 0 ? (
        <table className="w-full text-center flex justify-center item-center align-middle  table-auto ">
          <tbody className="border-separate border-spacing-2 border border-slate-500">
            <tr className="">
              <th className="m-0 p-5  border border-slate-500">Name</th>
              <th className="m-0 p-5  border border-slate-500">Age</th>
              <th className="m-0 p-5  border border-slate-500">Department</th>
              <th className="m-0 p-5  border border-slate-500">Address</th>
              <th className="m-0 p-5  border border-slate-500">changes</th>
            </tr>
            {data?.map((item: inputThing) => {
              return (
                <tr key={item._id}>
                  <td className="border border-slate-500">{item.Name}</td>
                  <td className="border border-slate-500">{item.Age}</td>
                  <td className="border border-slate-500">{item.Department}</td>
                  <td className="border border-slate-500">{item.Address}</td>
                  <td className="border border-slate-500">
                    {" "}
                    <Link
                      href={{
                        pathname: `/edit/${item._id}`,
                        query: item,
                      }}
                    >
                      Update 
                    </Link>
                    {" "}  / {" "}
                    <button onClick={(event) => handledelete(event, item)}>
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      ) : (
        <div className="w-full flex justify-center item-center align-middle">No Data avaialable</div>
      )}
    </div>
  );
}
