"use client"
import { useState, ChangeEvent, FormEvent } from 'react'

import Link from 'next/link'

interface inputThing{
    name:string,
    age:number | undefined,
    dept:string,
    address:string
}
const arr:inputThing[]=[]
export default function Home() {
  const [form,setForm]=useState<inputThing>({
    name:"",
    age:undefined,
    dept:"",
    address:"",
  })
  const handle=(e:ChangeEvent<HTMLInputElement>)=>{
    e.preventDefault();
   setForm({...form,[e.target.name]:e.target.value})
  }
  
  console.log(form);
  const handledata=async(e:FormEvent<HTMLFormElement>)=>{
    // arr.push(form)
    // console.log(arr)
   
    try {
      const response = await fetch("/api/prompt", {
        method: "POST",
        body: JSON.stringify({
          Name: form.name,
          Age: form.age,
          Department:form.dept,
          Address:form.address
        }),
        
      });

    } catch (error) {
      console.log("error",error);
    }

    

  } 
  return (
    <main className="min-h-screen  bg-gradient-to-r from-sky-500 to-indigo-500 w-full h-full">
     <div className='w-full flex  justify-center   a text-center  p-2'>
        <form className='justify-self-center' onSubmit={handledata}>
       <h1 className='font-bold text-lg'>Enter a Details</h1> 
        <input type='text' placeholder='Enter a name' name="name" value={form.name} onChange={handle} className='border-2 rounded-xl border-black m-2' /> <br/> 
        <input type="number" placeholder='Enter a age' name="age" value={form.age} onChange={handle} className='border-2 rounded-xl border-black m-2'  /><br/>
        <input type='text' placeholder='Enter a department' name="dept" value={form.dept} onChange={handle} className='border-2 m-2 rounded-xl border-black' /> <br/>
        <input type='text' placeholder='Enter a address' name="address" value={form.address} onChange={handle} className='border-2 m-2 rounded-xl border-black' /> <br/> 
        <button className='bg-red-500 text-white rounded-full w-36 hover:bg-sky-800'>submit</button> <br/><br/>
         
       </form>
    
 
     </div>


  
    </main>
  )
}
export {arr}
