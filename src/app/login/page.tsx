"use client"
import { useState, ChangeEvent, FormEvent } from 'react'
import { useRouter } from 'next/navigation'

interface inputThing{
   
    email:string,
    password:string
}
export default function Home() {
  const [form,setForm]=useState<inputThing>({
    email:"",
   password:""
  })
  const router=useRouter()
  const handle=(e:ChangeEvent<HTMLInputElement>)=>{
    e.preventDefault();
   setForm({...form,[e.target.name]:e.target.value})
  }
  
  const handledata=async(e:FormEvent<HTMLFormElement>)=>{
    e.preventDefault();

   try {
    const query={
        query:`{ login(email:"${form.email}",password:"${form.password}"){
          token
          message
        
        }}
        `
    }
    
      const response = await fetch("http://localhost:3001/graphql", {
        method: "POST",
        headers:{'content-type':'application/json'},
        body: JSON.stringify(query), 
      }).then((res)=>{
        return res.json()
      }).then((res)=>{
        console.log(res);
        if(res.errors){
            alert(res.errors[0].message)
            router.push("/login")

        }else{
            alert(res?.data.login?.message)
            router.push("/")
        } 
      })
    } catch (error) {
      console.log("error",error);
    }
  } 
  return (
    <div className="min-h-screen  bg-gradient-to-r from-orange-300 to-indigo-300 w-full h-full">
     <div className='w-full flex  justify-center   a text-center  p-2'>
        <form className='justify-self-center' onSubmit={handledata}>
       <h1 className='font-bold text-2xl capitalize'>login portal</h1> 
        <label htmlFor='E-mail' className='text-xl text-slate-900'>E-mail :</label>
        <input type="email" placeholder='  Enter a email' id='E-mail' name="email" value={form.email} onChange={handle} className='border-2 rounded-xl border-black m-2'  /><br/>
        <label htmlFor='password' className='text-xl text-slate-900'>password :</label>
        <input type='password' placeholder='  password..!' id='Password' name="password" value={form.password} onChange={handle} className='border-2 m-2 rounded-xl border-black' /> <br/>
        <button className='bg-red-500 text-white rounded-full w-36 hover:bg-sky-800'>submit</button> <br/><br/>
         
       </form>
    
 
     </div>


  
    </div>
  )
}

